<?php
	$settings = json_decode(file_get_contents(dirname(__FILE__)."/settings.json"), true);
	$options = array(
		'menu-delay',
		'section-delay-h1',
		'section-delay-h2',
		'section-delay-h3',
		'section-delay-p',
		'text-delay',
		'h1-font',
		'h2-font',
		'h3-font',
		'p-font',
		'font-size',
		'font-color',
		'background-color',
		'url',
		'text-delay',
		'menu-delay',
		'start-2-style',
		'start-2-times',
		'start-2-on',
		'start-2-off'
	);
	foreach($options as $option) if(!isset($settings[$option])) $settings[$option] = '';
	$fonts = array(
		"carnas" => "Carnas",
		"atari" => "Atari",
		"ataricc" => "Atari CC",
		"atarice" => "Atari CE",
		"atarics" => "Atari CS",
		"PressStart2P" => "Press Start 2P",
		"bitwise" => "Bitwise",
		"Jet Set" => "Jet Set",
		"notalot35" => "Notalot35",
		"whitrabt" => "Whitrabt",
		"Eraslght" => "Eras Light",
		"Helvetica" => "Helvetica",
		"Lucida Console" => "Lucida Console",
		"Courier New" => "Courier New"
	);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="en-US" xmlns="http://www.w3.org/1999/xhtml" dir="ltr" >
	<head>
	    <meta http-equiv="Content-type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=640" />
		<meta content='True' name='HandheldFriendly' />
		<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no' name='viewport' />
	</head>
	<body <?php foreach($options as $option): ?>data-<?php echo $option; ?>="<?php echo $settings[$option]; ?>"<?php endforeach; ?>>
		<ul class="menus">
		</ul>
		<div class="settings">
			<?php require_once("settings.partial.php"); ?>
		</div>
		<script type="text/x-tmpl" id="menuTemplate">
			{% if(o.content) { %}
				<li id="start-page-content">{%# o.content %}</li>
			{% } %}
			{% if(o.content2.content) { %}
				<li id="start-page-content" class="{%= o.content2.style %}" data-times="{%= o.content2.times %}" data-on="{%= o.content2.on %}" data-off="{%= o.content2.off %}">{%# o.content2.content %}</li>
			{% } %}
			{% for(var i=0; i < o.menus.length; i++) { %}
				<li>
					<h2>{%= o.menus[i].name %}</h2>
					{%# o.menus[i].description %}
					<ul class="menu-items">
						{% for(var j=0; j < o.menus[i].items.length; j++) { %}
							<li {% if(!o.menus[i].items[j].available) { %}class="not-available"{% } %}>
								{% if(!o.menus[i].items[j].hideName) { %}<h3>{%= o.menus[i].items[j].name %}</h3>{% } %}
								{%# o.menus[i].items[j].content %}
							</li>
						{% } %}
					</ul>
				</li>
			{% } %}
		</script>
		<link rel="stylesheet" href="css/spectrum.css">
		<link rel="stylesheet" href="css/stylesheet.css">
		<script type="text/javascript" src="js/jquery.min.js"></script>
		<script type="text/javascript" src="js/jquery.writescreen.js"></script>
		<script type="text/javascript" src="js/tmpl.min.js"></script>
		<script type="text/javascript" src="js/spectrum.js"></script>
		<script type="text/javascript" src="js/ckeditor_v4/ckeditor.js"></script>
		<script type="text/javascript" src="js/ckeditor_v4/adapters/jquery.js"></script>
		<script type="text/javascript" src="js/functions.js"></script>
	</body>
</html>