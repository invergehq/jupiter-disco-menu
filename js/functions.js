(function ($) {
	var $doc = $(document),
		$win = $(window),
		baseUrl = null,
		fontSize = 1,
		data = null,
		settings = null,
		textDelay = 5,
		screenDelay = 10,
		functions = {
			loadData: function() {
				$.getJSON("data.json.php", function(d) {
					if(d != null) {
						if(d["updated"]) {
							window.location.reload(true);
						} else {
							if(JSON.stringify(d) !== JSON.stringify(data)) {
								data = d;
								functions.writeData(false);
							}
						}
					}
				});
			},
			loadSettings: function() {
				$.getJSON("settings.json", function(d) {
					if(settings == null) settings = d;
					else {
						if(JSON.stringify(d) !== JSON.stringify(settings)) {
							settings = d;
							var $body = $("body"),
							$form = $("form#settings-form");
							for(var name in settings) {
								$body.attr("data-"+name, settings[name]);
								if($form.find(":input[name='"+name+"']").hasClass("color")) {
									$form.find(":input[name='"+name+"']").spectrum("set", settings[name]);
								} else {
									$form.find(":input[name='"+name+"']").val(settings[name]);
								}
							}
							functions.applySettings(false);
						}
					}
				});
			},
			writeData: function(now) {
				var $body = $('body'),
				start = $("form#settings-form textarea[name='start-page-content']").val();
				start2 = $("form#settings-form textarea[name='start-page-content2']").val();
				start2Style = $body.attr("data-start-2-style"),
				start2Times = parseInt($body.attr('data-start-2-times')) || 0,
				start2On = parseInt($body.attr('data-start-2-on')) || 0,
				start2Off = parseInt($body.attr('data-start-2-off')) || 0;

				if(start && start.length == '') start = null;
				if(start2 && start2.length == '') start2 = null;

				$(".menus").writescreen("reload", tmpl("menuTemplate", {
					content: start,
					content2: {
						content: start2,
						style: start2Style,
						on: start2On,
						off: start2Off,
						times: start2Times
					},
					menus: data
				}), now);
			},
			applySettings: function() {
				var $body = $("body"),
				newSize = parseFloat($body.attr("data-font-size")),
				newColor = $body.attr("data-font-color"),
				backgroundColor = $body.attr("data-background-color"),
				start2Style = $body.attr("data-start-2-style"),
				start2Times = parseInt($body.attr('data-start-2-times')) || 0,
				start2On = parseInt($body.attr('data-start-2-on')) || 0,
				start2Off = parseInt($body.attr('data-start-2-off')) || 0,
				textDelay = parseInt($body.attr("data-text-delay")) || 0,
				sectionDelayH1 = parseInt($body.attr("data-section-delay-h1")) || 0,
				sectionDelayH2 = parseInt($body.attr("data-section-delay-h2")) || 0,
				sectionDelayH3 = parseInt($body.attr("data-section-delay-h3")) || 0,
				sectionDelayP = parseInt($body.attr("data-section-delay-p")) || 0,
				screenDelay = parseInt($body.attr("data-menu-delay")) || 0;

				if(newSize) fontSize = newSize;

				$("html,body").css({
					"font-size": fontSize+"em",
					"color": newColor,
					"background-color": backgroundColor,
					"font-family": $body.attr("data-p-font")
				});

				$body.append('<style type="text/css">h1 {font-family: "'+$body.attr("data-h1-font")+'";} h2 {font-family: "'+$body.attr("data-h2-font")+'";} h3 {font-family: "'+$body.attr("data-h3-font")+'";} body, p {font-family: "'+$body.attr("data-p-font")+'";}<style>');

				$(".blinking-cursor").css("background-color", newColor);

				if($(".menus").length) {
					functions.writeData(true);

					$(".menus").writescreen({
						textSpeed: textDelay,
						screenSpeed: screenDelay*1000,
						sectionSpeed: {
							h1: sectionDelayH1,
							h2: sectionDelayH2,
							h3: sectionDelayH3,
							p: sectionDelayP
						}
					});
				}
			}
		};

	function initPage() {
		var $body = $("body");

		if($(".menus").length) {
			$body.keyup(function(e) {
				if(e.which == 27) {
					var $settings = $(".settings"),
					$menus = $(".menus");
					if($settings.is(":visible")) {
						$settings.hide();
						$menus.show();
					} else {
						$settings.show();
						$menus.hide();
					}
				}
			});

			baseUrl = $body.attr("data-url");

			$.getJSON("data.json.php", function(d) {
				data = d;

				$(".menus").writescreen({
					textSelector: "h1, h2, h3, p"
				});

				functions.writeData(true);
				functions.applySettings();
			});

			setInterval(functions["loadData"], 5000);
		}

		setInterval(functions["loadSettings"], 5000);

		$("input.color").spectrum({
			preferredFormat: "hex",
			flat: true,
			showInput: true
		});

		$("textarea").ckeditor();

		$("#settings-form button.save").click(function() {
			var name = $(this).attr("name"),
			$form = $("form#settings-form");
			$form.find(".sp-choose").click();

			switch(name) {
				case "apply":
					$.post("save.php", $form.serialize(), function() {

					});
				case "test":
					$form.find(":input").each(function() {
						$body.attr("data-"+$(this).attr("name"), $(this).val());
					});
					functions.applySettings();
					$(".settings").hide();
					$(".menus").show();
			}
		});

		$("#settings-form button.apply").click(function() {
			var $option = $("form#settings-form select[name='preset'] option:selected");

			$("input.color[name='font-color']").spectrum("set", $option.attr("data-font-color"));
			$("input.color[name='background-color']").spectrum("set", $option.attr("data-background-color"));
		});
	}
	
	$doc.on('ready', function() {
		initPage();
	});
})(jQuery);