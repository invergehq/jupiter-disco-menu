(function($) {
	var methods = {
		defaults: {
			textSpeed: 10,
			sectionSpeed: {},
			screenSpeed: 5,
			data: null,
			formatter: function() {},
			timeout: null,
			startScreenIndex: 0,
			startTextIndex: 0,
			screenSelector: "",
			textSelector: "p"
	    },
		init: function(opt) {
	    	return this.each(function() {
	    		var $this = $(this),
	    			options = $.extend(methods.defaults, opt),
	    			data = $this.data("writescreen");
	    		
	    		if(!data) {
	    			data = {
	    				options: options,
	    				currentScreenIndex: options.startScreenIndex,
	    				currentItemIndex: 0,
	    				currentTextIndex: options.startTextIndex,
	    				screens: $this.children(options.screenSelector),
	    				reload: false,
	    				timeout: null,
	    				cursor: $('<span class="blinking-cursor"></span>')
	    			};

	    			$this.data("writescreen", data);

	    			methods['evaluate'].apply(this);
	    			if(data.screens.length) methods['start'].apply(this);
	    		} else {
	    			data.options = $.extend(data.options, opt);
	    		}
	    	});
		},
		evaluate: function() {
			var $this = $(this),
			data = $this.data("writescreen");

			if(data) {
				data.screens.find(data.options.textSelector).each(function() {
					$(this).data("content", $(this).text());
				});
			}
		},
		reload: function(content, immediate) {
			var $this = $(this),
			data = $this.data("writescreen");
			if(data) {
				if(immediate) {
	    			data.reload = false;
					methods["stop"].apply(this);
					$this.html(content);
					data.screens = $this.children(data.options.screenSelector);
	    			methods['evaluate'].apply(this);
	    			methods['start'].apply(this);
				} else {
					data.reload = content;
				}
			}
		},
		start: function() {
			var $this = $(this),
			data = $this.data("writescreen");

			if(data) {
				clearTimeout(data.timeout);
				data.currentScreenIndex = data.options.startScreenIndex;
				data.currentTextIndex = data.options.startTextIndex;
				methods['shownext'].apply(this);
			}
		},
		stop: function() {
			var $this = $(this),
			data = $this.data("writescreen");

			if(data) {
				clearTimeout(data.timeout);
				data.options.currentScreenIndex = 0;
				data.options.currentTextIndex = 0;
			}
		},
		shownext: function() {
			var $this = $(this),
			data = $this.data("writescreen");

			if(data) {
				if(data.currentScreenIndex == 0 && data.reload) {
					methods["reload"].apply(this, [data.reload, true]);
				} else {
					var screen = data.screens.eq(data.currentScreenIndex);

					data.screens.hide();
					var height = screen.outerHeight(false),
					winH = $(window).height();
					screen.css("marginBottom", (winH-height)/2);
					screen.show();

					if(screen.hasClass('blink')) {
						methods['blink'].apply($this, [{
							screen: screen,
							times: screen.data('times'),
							off: screen.data('off'),
							on: screen.data('on')
						}])
					} else {
						screen.find(data.options.textSelector).html("").addClass("no-text");
						data.currentItems = screen.find(data.options.textSelector);
						data.currentItemIndex = 0;
						data.currentTextIndex = 0;
						data.currentItems.eq(data.currentItemIndex).addClass("writing").removeClass("no-text");
						methods["write"].apply(this);
					}

					data.currentScreenIndex++;
					if(data.currentScreenIndex >= data.screens.length) data.currentScreenIndex = 0;
				}
			}
		},
		write: function() {
			var $this = $(this),
			data = $this.data("writescreen");

			if(data) {
				var currentItem = data.currentItems.eq(data.currentItemIndex),
				content = currentItem.data("content"),
				delay = data.options.textSpeed;

				currentItem.html(content.substring(0, data.currentTextIndex)).append(data.cursor);
				data.currentTextIndex++;

				if(data.currentTextIndex > content.length) {
					data.currentTextIndex = 0;
					data.currentItemIndex++;
					if(data.options.sectionSpeed[currentItem.prop('nodeName').toLowerCase()]) delay += data.options.sectionSpeed[currentItem.prop('nodeName').toLowerCase()];
				}

				if(data.currentItemIndex < data.currentItems.length) {
					currentItem.removeClass("writing");
					data.currentItems.eq(data.currentItemIndex).addClass("writing").removeClass("no-text");
					data.timeout = setTimeout(function() {
						methods["write"].apply($this);
					}, delay);
				} else {
					data.timeout = setTimeout(function() {
						methods["shownext"].apply($this);
					}, data.options.screenSpeed);
				}
			}
		},
		blink: function(opts) {
			var $this = $(this),
			data = $this.data("writescreen");

			if(data) {
				if(opts.times) {
					if(opts.screen.is(':visible')) {
						setTimeout(function() {
								opts.screen.hide();
								methods['blink'].apply($this, [{
									screen: opts.screen,
									times: opts.times-1,
									on: opts.on,
									off: opts.off
								}]);
							},
							opts.on
						);
					} else {
						setTimeout(function() {
								opts.screen.show();
								methods['blink'].apply($this, [{
									screen: opts.screen,
									times: opts.times,
									on: opts.on,
									off: opts.off
								}]);
							},
							opts.off
						);
					}
				} else {
					methods["shownext"].apply($this);
				}
			}
		}
	};
	$.fn.writescreen = function(method) {
		if(methods[method]) {
			return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
		} else if (typeof method === 'object' || ! method) {
			retVal = methods.init.apply(this, arguments);
			return retVal;
		} else {
			$.error('Method '+method+' does not exist on jQuery.writescreen');
		}    
	};
})(jQuery);