<?php
	$settings = json_decode(file_get_contents(dirname(__FILE__)."/settings.json"), true);
	$options = array(
		'menu-delay',
		'section-delay-h1',
		'section-delay-h2',
		'section-delay-h3',
		'section-delay-p',
		'text-delay',
		'h1-font',
		'h2-font',
		'h3-font',
		'p-font',
		'font-size',
		'font-color',
		'background-color',
		'url',
		'text-delay',
		'menu-delay',
		'start-2-style',
		'start-2-times',
		'start-2-on',
		'start-2-off'
	);
	foreach($options as $option) if(!isset($settings[$option])) $settings[$option] = '';

	$fonts = array(
		"carnas" => "Carnas",
		"atari" => "Atari",
		"ataricc" => "Atari CC",
		"atarice" => "Atari CE",
		"atarics" => "Atari CS",
		"PressStart2P" => "Press Start 2P",
		"bitwise" => "Bitwise",
		"Jet Set" => "Jet Set",
		"notalot35" => "Notalot35",
		"whitrabt" => "Whitrabt",
		"Eraslght" => "Eras Light",
		"Helvetica" => "Helvetica",
		"Lucida Console" => "Lucida Console",
		"Courier New" => "Courier New"
	);
?>
<form id="settings-form" action="/index.php" method="POST">
	<fieldset>
		<div class="row">
			<div class="col col-6">
				<p>
					<label for="url">Website URL</label>
					<input type="text" id="url" name="url" value="<?php echo $settings["url"]; ?>" />
				</p>
				<p>
					<label for="h1Font">Header 1 Font</label>
					<select id="h1Font" name="h1-font">
						<?php foreach($fonts as $key => $font): ?>
							<option value="<?php echo $key; ?>" <?php if($settings["h1-font"] == $key): ?>selected="true"<?php endif; ?>><?php echo $font; ?></option>
						<?php endforeach; ?>
					</select>
				</p>
				<p>
					<label for="h2Font">Header 2 Font</label>
					<select id="h2Font" name="h2-font">
						<?php foreach($fonts as $key => $font): ?>
							<option value="<?php echo $key; ?>" <?php if($settings["h2-font"] == $key): ?>selected="true"<?php endif; ?>><?php echo $font; ?></option>
						<?php endforeach; ?>
					</select>
				</p>
				<p>
					<label for="h3Font">Header 3 Font</label>
					<select id="h3Font" name="h3-font">
						<?php foreach($fonts as $key => $font): ?>
							<option value="<?php echo $key; ?>" <?php if($settings["h3-font"] == $key): ?>selected="true"<?php endif; ?>><?php echo $font; ?></option>
						<?php endforeach; ?>
					</select>
				</p>
				<p>
					<label for="pFont">Main Font</label>
					<select id="pFont" name="p-font">
						<?php foreach($fonts as $key => $font): ?>
							<option value="<?php echo $key; ?>" <?php if($settings["p-font"] == $key): ?>selected="true"<?php endif; ?>><?php echo $font; ?></option>
						<?php endforeach; ?>
					</select>
				</p>
				<p>
					<label for="fontSize">Font Size</label>
					<input type="text" id="fontSize" name="font-size" value="<?php echo $settings["font-size"]; ?>" />
				</p>
			</div>
			<div class="col col-6">
				<p>
					<label for="preset">Color Preset</label>
					<select name="preset">
						<option data-font-color="#00ff00" data-background-color="#242222">Green</option>
						<option data-font-color="#37d9e1" data-background-color="#131111">Blue</option>
						<option data-font-color="#dbe4e5" data-background-color="#1e1d1d">White</option>
						<option data-font-color="#dbd50a" data-background-color="#1e1d1d">Yellow</option>
					</select>
					<button class="apply" type="button">Apply Preset</button>
				</p>
				<p>
					<label for="fontColor">Font Color</label>
					<input type="text" class="color" name="font-color" value="<?php echo $settings["font-color"]; ?>" />
				</p>
				<p>
					<label for="fontColor">Background Color</label>
					<input type="text" class="color" name="background-color" value="<?php echo $settings["background-color"]; ?>" />
				</p>
			</div>
		</div>
		<div class="row">
			<div class="col col-12">
				<p>
					<label for="startPageContent">Start Page Content</label>
					<textarea name="start-page-content"><?php echo $settings["start-page-content"]; ?></textarea>
				</p>
			</div>
		</div>
		<div class="row">
			<div class="col col-9">
				<p>
					<label for="startPageContent2">Second Page Content</label>
					<textarea name="start-page-content2"><?php echo $settings["start-page-content2"]; ?></textarea>
				</p>
			</div>
			<div class="col col-3">
				<p>
					<label for="start2Style">Style</label>
					<select id="start2Style" name="start-2-style">
						<option value="scroll" <?php if('scroll' == $settings["start-2-style"]): ?>selected="true"<?Php endif; ?>>Scroll Text</option>
						<option value="blink" <?php if('blink' == $settings["start-2-style"]): ?>selected="true"<?Php endif; ?>>Flash</option>
					</select>
				</p>
				<p>
					<label for="start2Times">Flash Times</label>
					<input type="text" id="start2Times" name="start-2-times" value="<?php echo $settings["start-2-times"]; ?>" />
				</p>
				<p>
					<label for="start2On">Flash On<span class="note">milliseconds</span></label>
					<input type="text" id="start2On" name="start-2-on" value="<?php echo $settings["start-2-on"]; ?>" />
				</p>
				<p>
					<label for="start2Off">Flash Off<span class="note">milliseconds</span></label>
					<input type="text" id="start2Off" name="start-2-off" value="<?php echo $settings["start-2-off"]; ?>" />
				</p>
			</div>
		</div>
		<div class="row">
			<div class="col col-4">
				<p>
					<label for="screenDelay">Screen Delay<span class="note">seconds</span></label>
					<input type="text" id="screenDelay" name="menu-delay" value="<?php echo $settings["menu-delay"]; ?>" />
				</p>
				<p>
					<label for="textDelay">Text Delay<span class="note">milliseconds</span></label>
					<input type="text" id="textDelay" name="text-delay" value="<?php echo $settings["text-delay"]; ?>" />
				</p>
			</div>
			<div class="col col-4">
				<p>
					<label for="sectionDelay">Heading 1 Delay<span class="note">milliseconds</span></label>
					<input type="text" id="sectionDelayH1" name="section-delay-h1" value="<?php echo $settings["section-delay-h1"]; ?>" />
				</p>
				<p>
					<label for="sectionDelay">Heading 2 Delay<span class="note">milliseconds</span></label>
					<input type="text" id="sectionDelayH2" name="section-delay-h2" value="<?php echo $settings["section-delay-h2"]; ?>" />
				</p>
			</div>
			<div class="col col-4">
				<p>
					<label for="sectionDelay">Heading 3 Delay<span class="note">milliseconds</span></label>
					<input type="text" id="sectionDelayH3" name="section-delay-h3" value="<?php echo $settings["section-delay-h3"]; ?>" />
				</p>
				<p>
					<label for="sectionDelay">Paragraph Delay<span class="note">milliseconds</span></label>
					<input type="text" id="sectionDelayP" name="section-delay-p" value="<?php echo $settings["section-delay-p"]; ?>" />
				</p>
			</div>
		</div>
		<p>
			<button class="save" type="button" name="test">Test</button>
			<button class="save" type="button" name="apply">Apply &amp; Save</button>
		</p>
	</fieldset>
</form>
