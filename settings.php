<?php
	$settings = json_decode(file_get_contents(dirname(__FILE__)."/settings.json"), true);

	$fonts = array(
		"carnas" => "Carnas",
		"atari" => "Atari",
		"ataricc" => "Atari CC",
		"atarice" => "Atari CE",
		"atarics" => "Atari CS",
		"PressStart2P" => "Press Start 2P",
		"bitwise" => "Bitwise",
		"Jet Set" => "Jet Set",
		"notalot35" => "Notalot35",
		"whitrabt" => "Whitrabt",
		"Eraslght" => "Eras Light",
		"Helvetica" => "Helvetica",
		"Lucida Console" => "Lucida Console",
		"Courier New" => "Courier New"
	);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="en-US" xmlns="http://www.w3.org/1999/xhtml" dir="ltr" >
	<head>
	    <meta http-equiv="Content-type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=640" />
		<meta content='True' name='HandheldFriendly' />
		<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no' name='viewport' />
	</head>
	<body data-screen-delay="<?php echo $settings["menu-delay"]; ?>" data-text-delay="<?php echo $settings["text-delay"]; ?>" data-h1-font="<?php echo $settings["h1-font"]; ?>" data-h2-font="<?php echo $settings["h2-font"]; ?>" data-h3-font="<?php echo $settings["h3-font"]; ?>" data-p-font="<?php echo $settings["p-font"]; ?>" data-font-size="<?php echo $settings["font-size"]; ?>" data-font-color="<?php echo $settings["font-color"]; ?>" data-background-color="<?php echo $settings["background-color"]; ?>" data-url="<?php echo $settings["url"]; ?>" data-text-delay="<?php echo $settings["text-delay"]; ?>" data-screen-delay="<?php echo $settings["menu-delay"]; ?>">
		<?php require_once("settings.partial.php"); ?>
		<link rel="stylesheet" href="css/spectrum.css">
		<link rel="stylesheet" href="css/stylesheet.css">
		<script type="text/javascript" src="js/jquery.min.js"></script>
		<script type="text/javascript" src="js/jquery.writescreen.js"></script>
		<script type="text/javascript" src="js/tmpl.min.js"></script>
		<script type="text/javascript" src="js/spectrum.js"></script>
		<script type="text/javascript" src="js/ckeditor_v4/ckeditor.js"></script>
		<script type="text/javascript" src="js/ckeditor_v4/adapters/jquery.js"></script>
		<script type="text/javascript" src="js/functions.js"></script>
	</body>
</html>