<?php
if(file_exists(dirname(__FILE__)."/updated")) {
	unlink(dirname(__FILE__)."/updated");
	
	echo json_encode(array(
		"updated" => true
	));
} else {
	$settings = json_decode(file_get_contents(dirname(__FILE__)."/settings.json"), true);
	$filename = dirname(__FILE__)."/menu-data.dat";

	if(!file_exists($filename) || time() > 5+filemtime($filename)) {
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $settings["url"]."/menus.json");
		curl_setopt($ch, CURLOPT_FAILONERROR, true);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
		curl_setopt($ch, CURLOPT_AUTOREFERER, true);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER,true);
		curl_setopt($ch, CURLOPT_TIMEOUT, 10);
		$json = curl_exec($ch);
		if ($json === false) {
		    error_log("cURL error number:" .curl_errno($ch));
		    error_log("cURL error:" . curl_error($ch));
		} else{
			$httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

			if($httpCode != 404) {
				$menuData = json_decode($json, true);

				if(!empty($menuData)) file_put_contents($filename, json_encode($menuData));
			}
		}

		curl_close($ch);
	}

	$menuData = json_decode(file_get_contents($filename), true);

	echo json_encode($menuData);
}
?>