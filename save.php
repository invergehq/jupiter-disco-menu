<?php
	$settings = json_decode(file_get_contents(dirname(__FILE__)."/settings.json"), true);

	foreach($_POST as $key => $value) $settings[$key] = $value;

	file_put_contents(dirname(__FILE__)."/settings.json", json_encode($settings));
?>